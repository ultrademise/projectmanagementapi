﻿using Microsoft.Extensions.Logging;
using ProjectManagementAPI.ProjectManagement;
using ProjectManagementAPI.DataAccess;
using ProjectManagementAPI.Model;
using Moq;
using ProjectManagementAPI.ProjectManagement.Implementation;

namespace Tests.UnitTests
{
    [TestFixture]
    internal class SubtaskServiceTests
    {
        private Mock<ILogger<SubtaskService>> _loggerMock;
        private Mock<ISubtaskDataAccess> _subtaskDataAccessMock;
        private Mock<ISubtaskUpdateValidator> _subtaskUpdateValidatorMock;

        private ISubtaskService _service;

        private List<Subtask> _sampleSubtasks;

        [OneTimeSetUp]
        public void SetUp()
        {
            _loggerMock = new Mock<ILogger<SubtaskService>>(MockBehavior.Loose);
            _subtaskDataAccessMock = new Mock<ISubtaskDataAccess>(MockBehavior.Strict);
            _subtaskUpdateValidatorMock = new Mock<ISubtaskUpdateValidator>(MockBehavior.Strict);

            _service = new SubtaskService(
                _loggerMock.Object,
                _subtaskDataAccessMock.Object,
                _subtaskUpdateValidatorMock.Object);

            _sampleSubtasks = CreateSomeSampleSubtasks();
        }

        [TearDown]
        public void TearDown()
        {
            _loggerMock.Invocations.Clear();
            _subtaskDataAccessMock.Invocations.Clear();
            _subtaskUpdateValidatorMock.Invocations.Clear();
        }

        [Test]
        public void GetAll_HappyPath()
        {
            _subtaskDataAccessMock.Setup(t => t.FetchSubtasks()).Returns(_sampleSubtasks);

            var result = _service.GetSubtasks();

            Assert.That(result.Count, Is.EqualTo(4));
        }

        [Test]
        public void UpdateSubtask_WhenIdDoesNotExist()
        {
            _subtaskDataAccessMock.Setup(t => t.FindSubtask(12)).Returns((Subtask?)null);

            var result = _service.UpdateSubtask(12, "", "", 1, 10, new DateOnly(2022, 9, 25));

            Assert.That(result, Is.Null);
        }

        [Test]
        public void UpdateSubtask_WhenIdExists_AndValuesAre_Invalid()
        {
            _subtaskDataAccessMock.Setup(t => t.FindSubtask(12)).Returns(_sampleSubtasks[0]);
            _subtaskUpdateValidatorMock.Setup(t
                => t.SubtaskIsValidToUpdate(_sampleSubtasks[0], It.IsAny<Subtask>()))
                .Returns(ValidationResult.ValueOutOfRange);

            var result = _service.UpdateSubtask(12, "", "", 1, 10, new DateOnly(2022, 9, 25));

            Assert.That(result, Is.Null);
        }

        [Test]
        public void UpdateSubtask_WhenIdExists_AndValuesAre_Valid()
        {
            _subtaskDataAccessMock.Setup(t => t.FindSubtask(12)).Returns(_sampleSubtasks[0]);
            _subtaskUpdateValidatorMock.Setup(t
                => t.SubtaskIsValidToUpdate(_sampleSubtasks[0], It.IsAny<Subtask>()))
                .Returns(ValidationResult.Success);
            _subtaskDataAccessMock.Setup(t => t.UpdateSubtask(It.IsAny<Subtask>())).Returns(_sampleSubtasks[0]);

            var result = _service.UpdateSubtask(12, "", "", 1, 10, new DateOnly(2022, 9, 25));

            Assert.That(result, Is.EqualTo(_sampleSubtasks[0]));
        }

        private List<Subtask> CreateSomeSampleSubtasks()
        {
            var subtasks = new List<Subtask>
            {
                new Subtask(1, "first", "first subtask", 30, new DateOnly(2022, 9, 24), 1),
                new Subtask(2, "second", "second subtask", 40, new DateOnly(2022, 9, 25), 1),
                new Subtask(3, "third", "third subtask", 50, new DateOnly(2022, 9, 26), 1),
                new Subtask(4, "fourth", "fourth subtask",  60, new DateOnly(2022, 9, 27), 1)
            };

            return subtasks;
        }
    }
}
