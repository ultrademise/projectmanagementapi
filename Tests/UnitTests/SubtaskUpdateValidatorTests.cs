﻿using ProjectManagementAPI.ProjectManagement;
using ProjectManagementAPI.Model;
using ProjectManagementAPI.ProjectManagement.Implementation;

namespace Tests.UnitTests
{
    [TestFixture]
    internal class SubtaskUpdateValidatorTests
    {

        private SubtaskUpdateValidator _subtaskUpdater;

        [SetUp]
        public void SetUp()
        {
            _subtaskUpdater = new SubtaskUpdateValidator();
        }

        [Test]
        public void testSuccess_WhenProgressValid()
        {
            var subtask1 = new Subtask(5, "test", "test desc", 60, new DateOnly(2020, 9, 20), 1);
            var subtask2 = new Subtask(5, "test", "test desc", 90, new DateOnly(2020, 9, 20), 1);

            var result = _subtaskUpdater.SubtaskIsValidToUpdate(subtask1, subtask2);

            Assert.That(result, Is.EqualTo(ValidationResult.Success));
        }

        [Test]
        public void testSuccess_WhenProgressInvalid()
        {
            var subtask1 = new Subtask(5, "test", "test desc", 65, new DateOnly(2020, 9, 20), 1);
            var subtask2 = new Subtask(5, "test", "test desc", 160, new DateOnly(2020, 9, 20), 1);

            var result = _subtaskUpdater.SubtaskIsValidToUpdate(subtask1, subtask2);

            Assert.That(result, Is.EqualTo(ValidationResult.ValueOutOfRange));
        }

        [Test]
        public void testSuccess_WhenCompleted()
        {
            var subtask1 = new Subtask(5, "test", "test desc", 100, new DateOnly(2020, 9, 20), 1);
            var subtask2 = new Subtask(5, "test", "test desc", 70, new DateOnly(2020, 9, 20), 1);

            var result = _subtaskUpdater.SubtaskIsValidToUpdate(subtask1, subtask2);

            Assert.That(result, Is.EqualTo(ValidationResult.Immutable));
        }
    }
}
