**Project Management API**

is a RESTful service that provides CRUD operations on Project management models which are Projects, Tasks and Subtasks.

It is a .NET 6 core web api application, using a MySql database for the persistence.



Acknowledgements:

Not implemented due to time restrictions in order of importance:

More unit test coverage: I would at least cover all the business code (under the ProjectManagement/Implementation namespace), but the existing unit tests are adequate to demonstrate my style of writting unit tests.

ORM: Although i have worked in the past with Entity Framework with MS SQL, i would need some time and searching for online sources to set up an ORM with MySql, so i prefered the ugly but faster solution of MysqlConnector and raw sql statementes in the data access layer. Another ORM i would like to explore is Dapper.

User authentication/authorization: Again, i have 0 experience with that on .NET so i left it for last.