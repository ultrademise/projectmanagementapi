﻿using ProjectManagementAPI.Model;
using MySqlConnector;

namespace ProjectManagementAPI.DataAccess.Mysql
{
    public class BigTaskDataAccessRawMysql : IBigTaskDataAccess
    {

        private readonly MySqlConnection _mySqlConnection;
        private readonly ISubtaskDataAccess _subtaskDataAccess;

        public BigTaskDataAccessRawMysql(
            MySqlConnection mySqlConnection,
            ISubtaskDataAccess subtaskDataAccess)
        {
            _mySqlConnection = mySqlConnection;
            _subtaskDataAccess = subtaskDataAccess;
        }

        public List<BigTask> FetchBigTasks()
        {
            List<Subtask> allSubtasks = _subtaskDataAccess.FetchSubtasks();
            List<BigTask> bigTasks = new();

            _mySqlConnection.Open();
            var command = new MySqlCommand("SELECT * FROM tasks;", _mySqlConnection);
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                var currentBigTaskId = reader.GetInt64("id");

                bigTasks.Add(new BigTask(
                    currentBigTaskId,
                    reader.GetString("title"),
                    reader.GetString("description"),
                    reader.GetString("tag"),
                    reader.GetDateOnly("finish_date"),
                    reader.GetInt64("parent_project_id"),
                    allSubtasks.Where(s => s.ParentTaskId == currentBigTaskId).ToList()));
            }

            _mySqlConnection.Close();

            return bigTasks;
        }

        public List<BigTask> GetBigTasksOfProject(long projectId)
        {
            List<Subtask> allSubtasks = _subtaskDataAccess.FetchSubtasks();
            List<BigTask> bigTasks = new();

            _mySqlConnection.Open();
            var command = new MySqlCommand($"SELECT * FROM tasks WHERE parent_project_id = {projectId};", _mySqlConnection);
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                var currentBigTaskId = reader.GetInt64("id");

                bigTasks.Add(new BigTask(
                    currentBigTaskId,
                    reader.GetString("title"),
                    reader.GetString("description"),
                    reader.GetString("tag"),
                    reader.GetDateOnly("finish_date"),
                    reader.GetInt64("parent_project_id"),
                    allSubtasks.Where(s => s.ParentTaskId == currentBigTaskId).ToList()));
            }

            _mySqlConnection.Close();

            return bigTasks;
        }

        public BigTask? FindBigTask(long id)
        {
            _mySqlConnection.Open();

            var query = $"SELECT * FROM tasks WHERE id = {id};";

            var command = new MySqlCommand(query, _mySqlConnection);
            var reader = command.ExecuteReader();

            if (!reader.Read())
            {
                _mySqlConnection.Close();

                return null;
            }

            List<Subtask> subtasksOfBigTask = _subtaskDataAccess.GetSubtasksOfBigTask(id);

            var bigTask = new BigTask(
                    id,
                    reader.GetString("title"),
                    reader.GetString("description"),
                    reader.GetString("tag"),
                    reader.GetDateOnly("finish_date"),
                    reader.GetInt64("parent_project_id"),
                    subtasksOfBigTask);

            _mySqlConnection.Close();

            return bigTask;
        }

        public long CreateNewBigTask(string title, string description, string tag, long parentProjectId, DateOnly finishDate)
        {
            _mySqlConnection.Open();

            var query = "INSERT INTO tasks (title, description, tag, parent_project_id, finish_date) "
                + $" VALUES ('{title}','{description}','{tag}',{parentProjectId},'{finishDate:yyyy-M-d}');";
            var command = new MySqlCommand(query, _mySqlConnection);
            command.ExecuteNonQuery();

            var id = command.LastInsertedId;

            _mySqlConnection.Close();

            return id;
        }

        public bool DeleteBigTask(long id)
        { 
            _mySqlConnection.Open();

            var query = $"DELETE FROM tasks WHERE id = {id};";
            var command = new MySqlCommand(query, _mySqlConnection);
            var rowsAffected = command.ExecuteNonQuery();

            _mySqlConnection.Close();

            return rowsAffected == 1;
        }

        public BigTask UpdateBigTask(BigTask bigTask)
        {
            _mySqlConnection.Open();

            var query = $"UPDATE tasks SET title = '{bigTask.Title}', " +
                $"description = '{bigTask.Description}', tag = '{bigTask.Tag}', " +
                $"parent_task_id = {bigTask.ParentProjectId}, " +
                $"finish_date = '{bigTask.FinishDate:yyyy-M-d}'" +
                $" WHERE id = {bigTask.Id};";
            var command = new MySqlCommand(query, _mySqlConnection);
            command.ExecuteNonQuery();

            _mySqlConnection.Close();

            return bigTask;
        }
    }
}
