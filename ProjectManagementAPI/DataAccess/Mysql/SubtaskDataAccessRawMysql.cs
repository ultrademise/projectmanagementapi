﻿using ProjectManagementAPI.Model;
using MySqlConnector;

namespace ProjectManagementAPI.DataAccess.Mysql
{
    public class SubtaskDataAccessRawMysql : ISubtaskDataAccess
    {

        private MySqlConnection _mySqlConnection;

        public SubtaskDataAccessRawMysql(MySqlConnection mySqlConnection)
        {
            _mySqlConnection = mySqlConnection;
        }


        public List<Subtask> FetchSubtasks()
        {

            List<Subtask> subtasks = new List<Subtask>();

            _mySqlConnection.Open();
            var command = new MySqlCommand("SELECT * FROM subtasks;", _mySqlConnection);
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                subtasks.Add(new Subtask(
                    reader.GetInt64("id"),
                    reader.GetString("title"),
                    reader.GetString("description"),
                    reader.GetInt32("progress"),
                    reader.GetDateOnly("finish_date"),
                    reader.GetInt64("parent_task_id")));
            }

            _mySqlConnection.Close();

            return subtasks;
        }

        public List<Subtask> GetSubtasksOfBigTask(long bigTaskId)
        {

            List<Subtask> subtasks = new List<Subtask>();

            _mySqlConnection.Open();
            var command = new MySqlCommand($"SELECT * FROM subtasks WHERE parent_task_id = {bigTaskId};", _mySqlConnection);
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                subtasks.Add(new Subtask(
                    reader.GetInt32("id"),
                    reader.GetString("title"),
                    reader.GetString("description"),
                    reader.GetInt32("progress"),
                    reader.GetDateOnly("finish_date"),
                    reader.GetInt32("parent_task_id")));
            }

            _mySqlConnection.Close();

            return subtasks;
        }

        public Subtask? FindSubtask(long id)
        {
            _mySqlConnection.Open();

            var query = $"SELECT * FROM subtasks WHERE id = {id};";

            var command = new MySqlCommand(query, _mySqlConnection);
            var reader = command.ExecuteReader();

            if (!reader.Read())
            {
                _mySqlConnection.Close();

                return null;
            }
            var subtask = new Subtask(
                reader.GetInt32("id"),
                reader.GetString("title"),
                reader.GetString("description"),
                reader.GetInt32("progress"),
                reader.GetDateOnly("finish_date"),
                reader.GetInt32("parent_task_id"));

            _mySqlConnection.Close();

            return subtask;

        }

        public long CreateNewSubtask(string title, string description, long parentTaskId, int progress, DateOnly finishDate)
        {
            _mySqlConnection.Open();

            var query = "INSERT INTO subtasks (title, description, parent_task_id, progress, finish_date) "
                + $" VALUES ('{title}','{description}',{parentTaskId},{progress},'{finishDate.ToString("yyyy-M-d")}');";
            var command = new MySqlCommand(query, _mySqlConnection);
            command.ExecuteNonQuery();

            var id = command.LastInsertedId;

            _mySqlConnection.Close();

            return id;
        }

        public bool DeleteSubtask(long id)
        { 
            _mySqlConnection.Open();

            var query = $"DELETE FROM subtasks WHERE id = {id};";
            var command = new MySqlCommand(query, _mySqlConnection);
            var rowsAffected = command.ExecuteNonQuery();

            _mySqlConnection.Close();

            return rowsAffected == 1;
        }

        public Subtask UpdateSubtask(Subtask subtask)
        {
            _mySqlConnection.Open();

            var query = $"UPDATE subtasks SET title = '{subtask.Title}', " +
                $"description = '{subtask.Description}', parent_task_id = {subtask.ParentTaskId}, " +
                $"progress = {subtask.Progress}, finish_date = '{subtask.FinishDate.ToString("yyyy-M-d")}'" +
                $" WHERE id = {subtask.Id};";
            var command = new MySqlCommand(query, _mySqlConnection);
            command.ExecuteNonQuery();

            _mySqlConnection.Close();

            return subtask;
        }
    }
}
