﻿using ProjectManagementAPI.Model;
using MySqlConnector;

namespace ProjectManagementAPI.DataAccess.Mysql
{
    public class ProjectDataAccessRawMysql : IProjectDataAccess
    {

        private readonly MySqlConnection _mySqlConnection;
        private readonly IBigTaskDataAccess _bigTaskDataAccess;

        public ProjectDataAccessRawMysql(
            MySqlConnection mySqlConnection,
            IBigTaskDataAccess bigTaskDataAccess)
        {
            _mySqlConnection = mySqlConnection;
            _bigTaskDataAccess = bigTaskDataAccess;
        }

        public List<Project> FetchProjects()
        {
            List<BigTask> allBigTasks = _bigTaskDataAccess.FetchBigTasks();
            List<Project> projects = new();

            _mySqlConnection.Open();
            var command = new MySqlCommand("SELECT * FROM projects;", _mySqlConnection);
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                var currentProjectId = reader.GetInt64("id");

                projects.Add(new Project(
                    currentProjectId,
                    reader.GetString("title"),
                    reader.GetString("description"),
                    reader.GetBoolean("is_public"),
                    reader.GetDateOnly("finish_date"),
                    allBigTasks.Where(t => t.ParentProjectId == currentProjectId).ToList()));
            }

            _mySqlConnection.Close();

            return projects;
        }

        public Project? FindProject(long id)
        {
            _mySqlConnection.Open();

            var query = $"SELECT * FROM projects WHERE id = {id};";

            var command = new MySqlCommand(query, _mySqlConnection);
            var reader = command.ExecuteReader();

            if (!reader.Read())
            {
                _mySqlConnection.Close();

                return null;
            }

            List<BigTask> bigTasksOfProject = _bigTaskDataAccess.GetBigTasksOfProject(id);

            var currentProjectId = reader.GetInt64("id");

            var project = new Project(
                currentProjectId,
                reader.GetString("title"),
                reader.GetString("description"),
                reader.GetBoolean("is_public"),
                reader.GetDateOnly("finish_date"),
                bigTasksOfProject);

            _mySqlConnection.Close();

            return project;
        }

        public long CreateNewProject(string title, string description, bool isPublic, DateOnly finishDate)
        {
            _mySqlConnection.Open();

            var query = "INSERT INTO projects (title, description, is_public, finish_date) "
                + $" VALUES ('{title}','{description}','{(isPublic ? 1 : 0)}','{finishDate:yyyy-M-d}');";
            var command = new MySqlCommand(query, _mySqlConnection);
            command.ExecuteNonQuery();

            var id = command.LastInsertedId;

            _mySqlConnection.Close();

            return id;
        }

        public bool DeleteProject(long id)
        {
            _mySqlConnection.Open();

            var query = $"DELETE FROM projects WHERE id = {id};";
            var command = new MySqlCommand(query, _mySqlConnection);
            var rowsAffected = command.ExecuteNonQuery();

            _mySqlConnection.Close();

            return rowsAffected == 1;
        }

        public Project UpdateProject(Project project)
        {
            _mySqlConnection.Open();

            var query = $"UPDATE projects SET title = '{project.Title}', " +
                $"description = '{project.Description}', " +
                $"is_public = {(project.IsPublic ? 1 : 0)}, " +
                $"finish_date = '{project.FinishDate:yyyy-M-d}'" +
                $" WHERE id = {project.Id};";
            var command = new MySqlCommand(query, _mySqlConnection);
            command.ExecuteNonQuery();

            _mySqlConnection.Close();

            return project;
        }
    }
}
