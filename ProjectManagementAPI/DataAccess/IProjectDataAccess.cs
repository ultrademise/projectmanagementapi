﻿using ProjectManagementAPI.Model;

namespace ProjectManagementAPI.DataAccess
{
    public interface IProjectDataAccess
    {
        public List<Project> FetchProjects();

        public Project? FindProject(long id);

        public long CreateNewProject(string title, string description, bool isPublic, DateOnly finishDate);

        public Project UpdateProject(Project project);

        public bool DeleteProject(long id);
    }
}
