﻿using ProjectManagementAPI.Model;

namespace ProjectManagementAPI.DataAccess
{
    public interface IBigTaskDataAccess
    {
        public List<BigTask> FetchBigTasks();

        public List<BigTask> GetBigTasksOfProject(long projectId);

        public BigTask? FindBigTask(long id);

        public long CreateNewBigTask(string title, string description, string tag, long parentProjectId, DateOnly finishDate);

        public BigTask UpdateBigTask(BigTask bigTask);

        public bool DeleteBigTask(long id);
    }
}
