﻿create database pmdb;

create table pmdb.projects(
	id int auto_increment primary key,
	title varchar(64) NOT NULL,
	description varchar (128),
	finish_date date not null,
	is_public tinyint not null
);

create table pmdb.tasks(
	id int auto_increment primary key,
	title varchar(64) NOT NULL,
	description varchar (128),
	tag varchar(32),
	parent_project_id int NOT NULL,
	finish_date date not null,
	foreign key (parent_project_id) REFERENCES projects(id) on delete cascade
);

create table pmdb.subtasks(
	id int auto_increment primary key,
	title varchar(64) NOT NULL,
	description varchar (128),
	progress int not null,
	parent_task_id int NOT NULL,
	finish_date date not null,
	foreign key (parent_task_id) REFERENCES tasks(id) on delete cascade
);