﻿using ProjectManagementAPI.Model;

namespace ProjectManagementAPI.DataAccess
{
    public interface ISubtaskDataAccess
    {
        public List<Subtask> FetchSubtasks();

        public List<Subtask> GetSubtasksOfBigTask(long bigTaskId);

        public Subtask? FindSubtask(long id);

        public long CreateNewSubtask(string title, string description, long parentTaskId, int progress, DateOnly finishDate);

        public Subtask UpdateSubtask(Subtask subtask);

        public bool DeleteSubtask(long id);
    }
}
