﻿using ProjectManagementAPI.ProjectManagement;
using ProjectManagementAPI.DataAccess;
using ProjectManagementAPI.DataAccess.Mysql;
using ProjectManagementAPI.Presentation.DTO.Mapping;
using MySqlConnector;
using ProjectManagementAPI.ProjectManagement.Implementation;

namespace ProjectManagementAPI
{
    public static class DependencyInjector
    {
        public static void InjectDependencies(IServiceCollection services, ConfigurationManager configuration)
        {
            services.AddTransient(MySqlConnection => new MySqlConnection(configuration["ConnectionStrings:ConnectionStringMysql"]));

            services.AddScoped<ISubtaskUpdateValidator, SubtaskUpdateValidator>();
            services.AddScoped<ISubtaskDataAccess, SubtaskDataAccessRawMysql>();
            services.AddScoped<IBigTaskDataAccess, BigTaskDataAccessRawMysql>();
            services.AddScoped<IProjectDataAccess, ProjectDataAccessRawMysql>();
            services.AddScoped<ISubtaskService, SubtaskService>();
            services.AddScoped<IBigTaskService, BigTaskService>();
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<IDomainToViewModelMapper, DomainToViewModelMapper>();
        }
    }
}
