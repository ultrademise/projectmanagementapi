﻿using Microsoft.AspNetCore.Mvc;
using ProjectManagementAPI.ProjectManagement;
using ProjectManagementAPI.Presentation.DTO.Mapping;

namespace ProjectManagementAPI.Presentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TaskController : Controller
    {

        private readonly ILogger<TaskController> _logger;

        private readonly IBigTaskService _bigTaskService;
        private readonly IDomainToViewModelMapper _domainToViewModelMapper;

        public TaskController(
            ILogger<TaskController> logger,
            IBigTaskService bigTaskService,
            IDomainToViewModelMapper domainToViewModelMapper)
        {
            _logger = logger;
            _bigTaskService = bigTaskService;
            _domainToViewModelMapper = domainToViewModelMapper;
        }

        [HttpGet("getTasks")]
        public ActionResult<List<DTO.Task>> Get()
        {
            try
            {
                return Ok(_bigTaskService.GetBigTasks().Select(t
                    => _domainToViewModelMapper.ConvertBigTask(t)));
            }
            catch (Exception exc)
            {
                _logger.LogError($"Unable to process the get tasks request. Cause: {exc.Message}");

                return StatusCode(500);
            }
        }

        [HttpGet("getTasksByTag")]
        public ActionResult<List<DTO.Task>> GetByTag(string tag)
        {
            try
            {
                return Ok(_bigTaskService.GetBigTasksWithTag(tag).Select(t
                    => _domainToViewModelMapper.ConvertBigTask(t)));
            }
            catch (Exception exc)
            {
                _logger.LogError($"Unable to process the get tasks request. Cause: {exc.Message}");

                return StatusCode(500);
            }
        }

        [HttpGet("{id}")]
        public ActionResult<DTO.Task> Get(int id)
        {
            try
            {
                var bigTask = _bigTaskService.GetBigTask(id);

                return bigTask != null ? Ok(_domainToViewModelMapper.ConvertBigTask(bigTask))
                    : NotFound();
            }
            catch (Exception exc)
            {
                _logger.LogError($"Unable to process the get task request. Cause: {exc.Message}");

                return StatusCode(500);
            }
        }

        [HttpPost]
        public ActionResult<DTO.Task> Post(DTO.Task bigTask)
        {
            try
            {
                var createdBigTask = _bigTaskService.CreateBigTask(
                    bigTask.Title,
                    bigTask.Description,
                    bigTask.Tag,
                    bigTask.ParentProjectId,
                    DateOnly.Parse(bigTask.FinishDate));

                return createdBigTask != null ? Ok(_domainToViewModelMapper.ConvertBigTask(createdBigTask))
                    : BadRequest();
            }
            catch (Exception exc)
            {
                _logger.LogError($"Unable to process the post task request. Cause: {exc.Message}");

                return StatusCode(500);
            }
        }

        [HttpPut("{id}")]
        public ActionResult<DTO.Task> Put(int id, DTO.Task bigTask)
        {
            var updatedBigTask = _bigTaskService.UpdateBigTask(
                id,
                bigTask.Title,
                bigTask.Description,
                bigTask.Tag,
                bigTask.ParentProjectId,
                DateOnly.Parse(bigTask.FinishDate));

            return updatedBigTask != null ? Ok(_domainToViewModelMapper.ConvertBigTask(updatedBigTask))
                : BadRequest();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                var deleted = _bigTaskService.DeleteBigTask(id);

                return deleted ? Ok() : NotFound();
            }
            catch (Exception exc)
            {
                _logger.LogError($"Unable to process the post task request. Cause: {exc.Message}");

                return StatusCode(500);
            }
        }
    }
}
