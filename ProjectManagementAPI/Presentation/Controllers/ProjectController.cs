﻿using Microsoft.AspNetCore.Mvc;
using ProjectManagementAPI.ProjectManagement;
using ProjectManagementAPI.Presentation.DTO;
using ProjectManagementAPI.Presentation.DTO.Mapping;

namespace ProjectManagementAPI.Presentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProjectController : Controller
    {

        private readonly ILogger<ProjectController> _logger;

        private readonly IProjectService _projectService;
        private readonly IDomainToViewModelMapper _domainToViewModelMapper;

        public ProjectController(
            ILogger<ProjectController> logger,
            IProjectService projectService,
            IDomainToViewModelMapper domainToViewModelMapper)
        {
            _logger = logger;
            _projectService = projectService;
            _domainToViewModelMapper = domainToViewModelMapper;
        }

        [HttpGet]
        public ActionResult<List<Project>> Get()
        {
            try
            {
                return Ok(_projectService.GetProjects().Select(p
                    => _domainToViewModelMapper.ConvertProject(p)));
            }
            catch (Exception exc)
            {
                _logger.LogError($"Unable to process the get projects request. Cause: {exc.Message}");

                return StatusCode(500);
            }
        }

        [HttpGet("{id}")]
        public ActionResult<Project> Get(int id)
        {
            try
            {
                var project = _projectService.GetProject(id);

                return project != null ? Ok(_domainToViewModelMapper.ConvertProject(project))
                    : NotFound();
            }
            catch (Exception exc)
            {
                _logger.LogError($"Unable to process the get project request. Cause: {exc.Message}");

                return StatusCode(500);
            }
        }

        [HttpPost]
        public ActionResult<Project> Post(Project project)
        {
            try
            {
                var createdProject = _projectService.CreateProject(
                    project.Title,
                    project.Description,
                    project.Public,
                    DateOnly.Parse(project.FinishDate));

                return createdProject != null ? Ok(_domainToViewModelMapper.ConvertProject(createdProject))
                    : BadRequest();
            }
            catch (Exception exc)
            {
                _logger.LogError($"Unable to process the post project request. Cause: {exc.Message}");

                return StatusCode(500);
            }
        }

        [HttpPut("{id}")]
        public ActionResult<Project> Put(int id, Project project)
        {
            var updatedProject = _projectService.UpdateProject(
                id,
                project.Title,
                project.Description,
                project.Public,
                DateOnly.Parse(project.FinishDate));

            return updatedProject != null ? Ok(_domainToViewModelMapper.ConvertProject(updatedProject))
                : BadRequest();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                var deleted = _projectService.DeleteProject(id);

                return deleted ? Ok() : NotFound();
            }
            catch (Exception exc)
            {
                _logger.LogError($"Unable to process the delete project request. Cause: {exc.Message}");

                return StatusCode(500);
            }
        }
    }
}
