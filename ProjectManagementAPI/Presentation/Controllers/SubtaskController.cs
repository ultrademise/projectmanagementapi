﻿using Microsoft.AspNetCore.Mvc;
using ProjectManagementAPI.ProjectManagement;
using ProjectManagementAPI.Presentation.DTO;
using ProjectManagementAPI.Presentation.DTO.Mapping;

namespace ProjectManagementAPI.Presentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SubtaskController : Controller
    {

        private readonly ILogger<SubtaskController> _logger;

        private readonly ISubtaskService _subtaskService;
        private readonly IDomainToViewModelMapper _domainToViewModelMapper;

        public SubtaskController(
            ILogger<SubtaskController> logger,
            ISubtaskService subtaskService,
            IDomainToViewModelMapper domainToViewModelMapper)
        {
            _logger = logger;
            _subtaskService = subtaskService;
            _domainToViewModelMapper = domainToViewModelMapper;
        }

        [HttpGet]
        public ActionResult<List<Subtask>> Get()
        {
            try
            {
                return Ok(_subtaskService.GetSubtasks().Select(t
                    => _domainToViewModelMapper.ConvertSubtask(t)));
            }
            catch (Exception exc)
            {
                _logger.LogError($"Unable to process the get subtasks request. Cause: {exc.Message}");

                return StatusCode(500);
            }
        }

        [HttpGet("{id}")]
        public ActionResult<Subtask> Get(int id)
        {
            try
            {
                var subtask = _subtaskService.GetSubtask(id);

                return subtask != null ? Ok(_domainToViewModelMapper.ConvertSubtask(subtask))
                    : NotFound();
            }
            catch (Exception exc)
            {
                _logger.LogError($"Unable to process the get subtask request. Cause: {exc.Message}");

                return StatusCode(500);
            }
        }

        [HttpPost]
        public ActionResult<Subtask> Post(Subtask subtask)
        {
            try
            {
                var createdSubtask = _subtaskService.CreateSubtask(
                    subtask.Title,
                    subtask.Description,
                    subtask.ParentTaskId,
                    DateOnly.Parse(subtask.FinishDate));

                return createdSubtask != null ? Ok(_domainToViewModelMapper.ConvertSubtask(createdSubtask))
                    : BadRequest();
            }
            catch (Exception exc)
            {
                _logger.LogError($"Unable to process the post subtask request. Cause: {exc.Message}");

                return StatusCode(500);
            }
        }

        [HttpPut("{id}")]
        public ActionResult<Subtask> Put(int id, Subtask subtask)
        {
            var updatedSubtask = _subtaskService.UpdateSubtask(
                id,
                subtask.Title,
                subtask.Description,
                subtask.ParentTaskId,
                subtask.Progress,
                DateOnly.Parse(subtask.FinishDate));

            return updatedSubtask != null ? Ok(_domainToViewModelMapper.ConvertSubtask(updatedSubtask))
                : BadRequest();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                var deleted = _subtaskService.DeleteSubtask(id);

                return deleted ? Ok() : NotFound();
            }
            catch (Exception exc)
            {
                _logger.LogError($"Unable to process the post subtask request. Cause: {exc.Message}");

                return StatusCode(500);
            }
        }
    }
}
