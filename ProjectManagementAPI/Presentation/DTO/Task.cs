﻿namespace ProjectManagementAPI.Presentation.DTO
{
    public record Task(
        long? Id,
        string Title,
        string Description,
        string Tag,
        int? Progress,
        string FinishDate,
        long ParentProjectId,
        List<Subtask>? Subtasks);
}
