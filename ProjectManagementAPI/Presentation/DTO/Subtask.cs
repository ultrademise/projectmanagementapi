﻿namespace ProjectManagementAPI.Presentation.DTO
{
    public record Subtask(
        long? Id,
        string Title,
        string Description,
        int Progress,
        string FinishDate,
        long ParentTaskId);
}
