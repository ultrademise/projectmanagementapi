﻿namespace ProjectManagementAPI.Presentation.DTO
{
    public record Project(
        long? Id,
        string Title,
        string Description,
        bool Public,
        int? Progress,
        string FinishDate,
        List<Task>? Tasks);
}
