﻿namespace ProjectManagementAPI.Presentation.DTO.Mapping
{
    public class DomainToViewModelMapper : IDomainToViewModelMapper
    {
        public Subtask ConvertSubtask(Model.Subtask subtask)
        {
            return new Subtask(
                subtask.Id,
                subtask.Title,
                subtask.Description,
                subtask.Progress,
                subtask.FinishDate.ToString(),
                subtask.ParentTaskId);
        }

        public Task ConvertBigTask(Model.BigTask bigTask)
        {
            return new Task(
                bigTask.Id,
                bigTask.Title,
                bigTask.Description,
                bigTask.Tag,
                bigTask.Progress(),
                bigTask.FinishDate.ToString(),
                bigTask.ParentProjectId,
                bigTask.Subtasks.Select(s => ConvertSubtask(s)).ToList());
        }

        public Project ConvertProject(Model.Project project)
        {
            return new Project(
                project.Id,
                project.Title,
                project.Description,
                project.IsPublic,
                project.Progress(),
                project.FinishDate.ToString(),
                project.Tasks.Select(t => ConvertBigTask(t)).ToList());
        }
    }
}
