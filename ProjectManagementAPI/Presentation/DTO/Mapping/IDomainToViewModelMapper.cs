﻿namespace ProjectManagementAPI.Presentation.DTO.Mapping
{
    public interface IDomainToViewModelMapper
    {
        Subtask ConvertSubtask(Model.Subtask subtask);

        Task ConvertBigTask(Model.BigTask bigTask);

        Project ConvertProject(Model.Project project);
    }
}
