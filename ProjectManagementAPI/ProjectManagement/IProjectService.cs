﻿using ProjectManagementAPI.Model;


namespace ProjectManagementAPI.ProjectManagement
{
    public interface IProjectService
    {
        List<Project> GetProjects();

        Project? GetProject(long id);

        Project? CreateProject(string title, string description, bool isPublic, DateOnly finishDate);

        Project? UpdateProject(long id, string title, string description, bool isPublic, DateOnly finishDate);

        bool DeleteProject(long id);
    }
}
