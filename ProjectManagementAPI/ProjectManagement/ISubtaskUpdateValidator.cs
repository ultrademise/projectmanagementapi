﻿using ProjectManagementAPI.Model;


namespace ProjectManagementAPI.ProjectManagement
{
    public interface ISubtaskUpdateValidator
    {
        public ValidationResult SubtaskIsValidToUpdate(Subtask storedSubtask, Subtask updatedSubtask);
    }
}
