﻿using ProjectManagementAPI.Model;


namespace ProjectManagementAPI.ProjectManagement
{
    public interface ISubtaskService
    {
        List<Subtask> GetSubtasks();

        Subtask? GetSubtask(long id);

        Subtask? CreateSubtask(string title, string description, long parentTaskId, DateOnly finishDate);

        Subtask? UpdateSubtask(long id, string title, string description, long parentTaskId, int progress, DateOnly finishDate);

        bool DeleteSubtask(long id);
    }
}
