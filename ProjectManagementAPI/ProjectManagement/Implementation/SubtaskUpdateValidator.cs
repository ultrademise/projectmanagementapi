﻿using ProjectManagementAPI.Model;

namespace ProjectManagementAPI.ProjectManagement.Implementation
{
    public class SubtaskUpdateValidator : ISubtaskUpdateValidator
    {
        public ValidationResult SubtaskIsValidToUpdate(Subtask storedSubtask, Subtask updatedSubtask)
        {
            if (storedSubtask.Progress == 100)
            {
                return ValidationResult.Immutable;
            }

            if (updatedSubtask.Progress > 100 || updatedSubtask.Progress < 0)
            {
                return ValidationResult.ValueOutOfRange;
            }

            if (storedSubtask.Id != updatedSubtask.Id)
            {
                return ValidationResult.NotFound;
            }

            return ValidationResult.Success;
        }
    }
}
