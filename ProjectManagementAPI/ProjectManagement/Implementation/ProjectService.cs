﻿using ProjectManagementAPI.Model;
using ProjectManagementAPI.DataAccess;

namespace ProjectManagementAPI.ProjectManagement.Implementation
{
    public class ProjectService : IProjectService
    {

        private readonly IProjectDataAccess _projectDataAccess;

        public ProjectService(IProjectDataAccess projectDataAccess)
        {
            _projectDataAccess = projectDataAccess;
        }

        public Project? GetProject(long id)
        {
            return _projectDataAccess.FindProject(id);
        }

        public List<Project> GetProjects()
        {
            return _projectDataAccess.FetchProjects();
        }

        public Project? CreateProject(string title, string description, bool isPublic, DateOnly finishDate)
        {
            var id = _projectDataAccess.CreateNewProject(title, description, isPublic, finishDate);

            return _projectDataAccess.FindProject(id);
        }

        public bool DeleteProject(long id)
        {
            return _projectDataAccess.DeleteProject(id);
        }

        public Project? UpdateProject(long id, string title, string description, bool isPublic, DateOnly finishDate)
        {
            var project = _projectDataAccess.FindProject(id);

            return project == null ? CreateProject(title, description, isPublic, finishDate)
            : _projectDataAccess.UpdateProject(project);
        }
    }
}
