﻿using ProjectManagementAPI.Model;
using ProjectManagementAPI.DataAccess;

namespace ProjectManagementAPI.ProjectManagement.Implementation
{
    public class BigTaskService : IBigTaskService
    {

        private readonly IBigTaskDataAccess _bigTaskDataAccess;

        public BigTaskService(IBigTaskDataAccess bigTaskDataAccess)
        {
            _bigTaskDataAccess = bigTaskDataAccess;
        }

        public BigTask? GetBigTask(long id)
        {
            return _bigTaskDataAccess.FindBigTask(id);
        }

        public List<BigTask> GetBigTasks()
        {
            return _bigTaskDataAccess.FetchBigTasks();
        }

        public List<BigTask> GetBigTasksWithTag(string tag)
        {
            return GetBigTasks().Where(t => t.Tag.Equals(tag)).ToList();
        }

        public BigTask? CreateBigTask(string title, string description, string tag, long parentProjectId, DateOnly finishDate)
        {
            var id = _bigTaskDataAccess.CreateNewBigTask(title, description, tag, parentProjectId, finishDate);

            return _bigTaskDataAccess.FindBigTask(id);
        }

        public bool DeleteBigTask(long id)
        {
            return _bigTaskDataAccess.DeleteBigTask(id);
        }

        public BigTask? UpdateBigTask(long id, string title, string description, string tag, long parentProjectId, DateOnly finishDate)
        {
            var bigTask = _bigTaskDataAccess.FindBigTask(id);

            return bigTask == null ? CreateBigTask(title, description, tag, parentProjectId, finishDate)
            : _bigTaskDataAccess.UpdateBigTask(bigTask);
        }
    }
}
