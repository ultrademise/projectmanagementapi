﻿using ProjectManagementAPI.Model;
using ProjectManagementAPI.DataAccess;

namespace ProjectManagementAPI.ProjectManagement.Implementation
{
    public class SubtaskService : ISubtaskService
    {
        private ILogger<SubtaskService> _logger;

        private ISubtaskDataAccess _subtaskDataAccess;
        private ISubtaskUpdateValidator _updateValidator;

        public SubtaskService(
            ILogger<SubtaskService> logger,
            ISubtaskDataAccess subtaskDataAccess,
            ISubtaskUpdateValidator subtaskUpdateValidator)
        {
            _logger = logger;
            _subtaskDataAccess = subtaskDataAccess;
            _updateValidator = subtaskUpdateValidator;
        }

        public Subtask? GetSubtask(long id)
        {
            return _subtaskDataAccess.FindSubtask(id);
        }

        public Subtask? CreateSubtask(string title, string description, long parentTaskId, DateOnly finishDate)
        {
            var id = _subtaskDataAccess.CreateNewSubtask(title, description, parentTaskId, 0, finishDate);

            return _subtaskDataAccess.FindSubtask(id);
        }

        public bool DeleteSubtask(long id)
        {
            return _subtaskDataAccess.DeleteSubtask(id);
        }

        List<Subtask> ISubtaskService.GetSubtasks()
        {
            return _subtaskDataAccess.FetchSubtasks();
        }

        public Subtask? UpdateSubtask(long id, string title, string description, long parentTaskId, int progress, DateOnly finishDate)
        {
            var subtask = _subtaskDataAccess.FindSubtask(id);

            if (subtask == null)
            {
                return null;
            }

            var updatedSubtask = new Subtask(subtask.Id, title, description, progress, finishDate, parentTaskId);
            var updateValidationResult = _updateValidator.SubtaskIsValidToUpdate(subtask, updatedSubtask);


            if (updateValidationResult != ValidationResult.Success)
            {
                _logger.LogInformation($"Cannot update sutbask. Validation result is {updateValidationResult}");

                return null;
            }

            return _subtaskDataAccess.UpdateSubtask(updatedSubtask);
        }
    }
}
