﻿using ProjectManagementAPI.Model;


namespace ProjectManagementAPI.ProjectManagement
{
    public interface IBigTaskService
    {
        List<BigTask> GetBigTasks();

        List<BigTask> GetBigTasksWithTag(string tag);

        BigTask? GetBigTask(long id);

        BigTask? CreateBigTask(string title, string description, string tag, long parentProjectId, DateOnly finishDate);

        BigTask? UpdateBigTask(long id, string title, string description, string tag, long parentProjectId, DateOnly finishDate);

        bool DeleteBigTask(long id);
    }
}
