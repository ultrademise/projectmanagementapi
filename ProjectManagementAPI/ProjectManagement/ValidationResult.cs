﻿namespace ProjectManagementAPI.ProjectManagement
{
    public enum ValidationResult
    {
        Success,
        Immutable,
        ValueOutOfRange,
        NotFound
    }

}
