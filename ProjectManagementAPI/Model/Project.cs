﻿namespace ProjectManagementAPI.Model
{
    public class Project
    {
        public Project(
            long id,
            string title,
            string description,
            bool isPublic,
            DateOnly finishDate,
            List<BigTask> tasks)
        {
            Id = id;
            Title = title;
            Description = description;
            IsPublic = isPublic;
            FinishDate = finishDate;
            Tasks = tasks;
        }

        public long Id { get; init; }
        public string Title { get; init; }
        public string Description { get; init; }
        public bool IsPublic { get; init; }
        public DateOnly FinishDate { get; init; }
        public List<BigTask> Tasks { get; init; }

        public int Progress()
        {
            return Tasks.Count == 0 ? 0
                : Tasks.Select(x => x.Progress()).Sum() / Tasks.Count;
        }
    }
}
