﻿namespace ProjectManagementAPI.Model
{
    public class BigTask
    {
        public BigTask(long id, string title, string description, string tag, DateOnly finishDate, long parentProjectId, List<Subtask> subtasks)
        {
            Id = id;
            Title = title;
            Description = description;
            Tag = tag;
            Subtasks = subtasks;
            FinishDate = finishDate;
            ParentProjectId = parentProjectId;
        }

        public long Id { get; init; }
        public string Title { get; init; }
        public string Description { get; init; }
        public string Tag { get; init; }
        public DateOnly FinishDate { get; init; }
        public long ParentProjectId { get; init; }
        public List<Subtask> Subtasks { get; init; }

        public int Progress()
        {
            return Subtasks.Count == 0 ? 0
                : Subtasks.Select(x => x.Progress).Sum() / Subtasks.Count;
        }
    }
}
