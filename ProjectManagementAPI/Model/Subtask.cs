﻿namespace ProjectManagementAPI.Model
{
    public class Subtask
    {
        public Subtask(long id, string title, string description, int progress, DateOnly finishDate, long parentTaskId)
        {
            Id = id;
            Title = title;
            Description = description;
            Progress = progress;
            FinishDate = finishDate;
            ParentTaskId = parentTaskId;
        }

        public long Id { get; init; }
        public string Title { get; init; }
        public string Description { get; init; }
        public int Progress { get; init; }
        public DateOnly FinishDate { get; init; }
        public long ParentTaskId { get; init; }
    }
}
